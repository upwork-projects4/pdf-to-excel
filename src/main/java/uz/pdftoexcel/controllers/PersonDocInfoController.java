package uz.pdftoexcel.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdftoexcel.services.PdfToExcelService;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class PersonDocInfoController {
    private final PdfToExcelService pdfToExcelService;

    @GetMapping("pdf-to-excel-write")
    void getPdfToExcelWrite() {
        try {
            pdfToExcelService.getPdfToExcelWrite();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @GetMapping("pdf-to-excel-read")
    void getPdfToExcel() {
        try {
            pdfToExcelService.getReadPdf();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
