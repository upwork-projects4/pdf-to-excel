package uz.pdftoexcel.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.StringReader;

@Service
public class PdfToExcelService {
    private static final String FILE_NAME = "e:/pn/itext.pdf";
    private static final String FILE_NAME1 = "e:/pn/1.pdf";

    public void getPdfToExcelWrite() {
        Document document = new Document();

        try {

            PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));

            //open
            document.open();

            Paragraph p = new Paragraph();
            p.add("This is my paragraph 1");
            p.setAlignment(Element.ALIGN_CENTER);

            document.add(p);

            Paragraph p2 = new Paragraph();
            p2.add("This is my paragraph 2"); //no alignment

            document.add(p2);

            Font f = new Font();
            f.setStyle(Font.BOLD);
            f.setSize(8);

            document.add(new Paragraph("This is my paragraph 3", f));

            //close
            document.close();

            System.out.println("Done");

        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }

    }

    public void getPdfToExcelRead() {
        StringBuilder str = new StringBuilder();
        try {
            PdfReader reader = new PdfReader(FILE_NAME1);
            int n = reader.getNumberOfPages();
            for (int i = 1; i <= 1; i++) {
                String str2 = PdfTextExtractor.getTextFromPage(reader, i);
                str.append(str2);
                System.out.println(str);
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
//        return String.format("%s", str);
    }
    public void getReadPdf() {

    }

}
